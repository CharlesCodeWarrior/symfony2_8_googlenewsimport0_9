<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use DOMDocument;

class GoogleNewsImportController extends Controller
{


    /** Returns an array form a Google Feed
     *
     * @param $name
     * @return an array with the contents of Google Feed for Outputting; in Twig for example
     * array()
     * [title, link e.g.] should be self explaining, example $NewsItem['title'];
     * ["description"] : here Google puts out raw html text
     * ["shortDescription"] : shortenend Version of description wihtout html junk, for output in twig etc.
     *      using a max lenght of 200 is advised
     * descriptionPlainArray : Future use: Extracted  Description paragraphs with fonts
     * $NewsItemsArray[$key]["imgSrc"] = $imgSrc;
     * @Route("/feed/google")
     */
    public function indexAction()
    {

        /**#explaining/testing output of the algorithm, depth is the, current level depth in DOM-Tree,
         * this is just for identing and understanding alogrithm
         */
        function test_XML_to_Array_explain($depth, $nodeName, $textContent)
        {
            //#explaining/testing output of the algorithm, depth is the, current level depth in DOM-Tree,
            // this is just for identing and understanding alogrithm
            $code = '<br />';
            for ($i = 0; $i < $depth; $i++) {
                $code += '_';
            }
            $code += (' XML_Node: ' . $nodeName);
            if (isset($textContent) && !empty($textContent)) {
                echo '  ,text: ' . $textContent;
            }
            $code += (' ,depth: ' . $depth);
            $code += '<br />';
            $depth++;
            //echo $code;
            return $depth;
        }

        /** xml (DOMDocument) import to array, iterative recursive algorithm, parsing the DOM-Tree"
         * @param $rootXML xml (DOMDocument)
         * @param $depth current level depth in DOM-Tree
         * @return array|mixed array with the imported xml content
         */
        function xml_to_array($rootXML, $depth)
        {
            $result = array();
            /** @var \DOMElement $rootXML */
            if ($rootXML->hasAttributes()) {
                //attributes important if nothing in TextContent especially
                $attrs = $rootXML->attributes;
                foreach ($attrs as $attr) {
                    $result['@attributes'][$attr->name] = $attr->value;
                }
            }

            if ($rootXML->hasChildNodes()) {
                //more Child Nodes present, potential recursion begins
                $childrenNodes = $rootXML->childNodes;
                //$depth = test_XML_to_Array_explain ($depth, $rootXML->nodeName, $rootXML->textContent);

                if ($childrenNodes->length == 1) {
                    $childNode = $childrenNodes->item(0);
                    if ($childNode->nodeType == XML_TEXT_NODE) {
                        $result['_value'] = $childNode->nodeValue;
                        //recursion check , end of recursion
                        //either return 1 or the whole result
                        return count($result) == 1
                            ? $result['_value']
                            : $result;
                    }
                }
                $groups = array();
                //a flat foreach loop for parsing childrenNodes /siblings
                //this not going a depth deeper
                foreach ($childrenNodes as $childNode) {
                    if (!isset($result[$childNode->nodeName])) {
                        $result[$childNode->nodeName] = xml_to_array($childNode, $depth);
                    } else {
                        if (!isset($groups[$childNode->nodeName])) {
                            $result[$childNode->nodeName] = array($result[$childNode->nodeName]);
                            $groups[$childNode->nodeName] = 1;
                        }


                        //recursive parsing of childrenNode list,
                        $result[$childNode->nodeName][] = xml_to_array($childNode, $depth);
                    }
                }
            } else {
                //$depth = test_XML_to_Array_explain ($depth, $rootXML->nodeName,  $rootXML->textContent);
            }
            return $result;
        }

        function searchNewsItems($pNewsItemsArray)
        {
            $NewsItemsArray = $pNewsItemsArray;

            foreach ($NewsItemsArray as $key => $NewsItem) {
                if (isset($NewsItem['description'])) {
                    $descr = $NewsItem['description'];
                    //extract plain text in description/artikel erste drei Zeilen /Beschreibung rausziehen
                    $textWithFont = strip_tags($descr, '<font></font>');
                    //echo '<br />  <br /> $textWithFont: '.htmlspecialchars($textWithFont); echo '<br />  <br />';

                    //extract short descripption without html junk
                    /* preg_match_all match the regexp in all the $html string and output everything as
                   an array in $result. "i" option is used to make it case insensitive */
                    preg_match_all('/<font size=[^>]+>[^>]+<\/font>/i', $textWithFont, $fontsArr);
                    //echo '<br />  <br /> fontsArr: '.var_dump($fontsArr[0][1]); echo '<br />  <br />';
                    $sourceOfNewsQuelle = $fontsArr[0][0];
                    $sourceOfNewsQuelle = strip_tags($sourceOfNewsQuelle);
                    $shortDescription = $fontsArr[0][1];
                    $shortDescription = strip_tags($shortDescription);
                    $NewsItemsArray[$key]["shortDescription"] = $shortDescription;
                    $NewsItemsArray[$key]["sourceOfNewsQuelle"] = $sourceOfNewsQuelle;

                    foreach ($fontsArr as $keyFonts => $fontsItem) {
                        $NewsItemsArray[$key]["descriptionPlainArray"][$keyFonts] = $fontsItem;
                    }

                    //img, extract img with regexp. domdoc not possible here.
                    preg_match_all('/<img(.*)src(.*)=(.*)\"(.*)\"/U', $descr, $resultsArrImg);
                    $imgSrc = array_pop($resultsArrImg);
                    $NewsItemsArray[$key]["imgSrc"] = $imgSrc;
                    unset($NewsItemsArray[$key]['description']);

                    //raw img
                    preg_match_all('/<img[^>]+>/i', $descr, $resultsArr);
                    foreach ($resultsArr as $keyImg => $resItem) {
                        //use NewsItemsArray[0]["imgArray"][0][0]
                        $NewsItemsArray[$key]["imgArray"][$keyImg] = $resItem;
                    }


                }
            }
            return $NewsItemsArray;
        }


        //#feeds/files
        $googleNewsXML = file_get_contents('https://news.google.de/news/feeds?pz=1&cf=all&hl=de&topic=b&output=rss');
        /** @var \DOMElement $googleNewsDOM */
        $googleNewsDOM = DOMDocument::loadXML($googleNewsXML, LIBXML_PARSEHUGE);
        $xmlResultArray = xml_to_array($googleNewsDOM, 0);


        if (isset($xmlResultArray) && !empty($xmlResultArray)) {
            if (isset ($xmlResultArray['rss']['channel']['item']) && !empty($xmlResultArray['rss']['channel']['item'])) {
                //$NewsItemsArray[] enthält bei korrekter Konfiguration das Ergebnis/ Die News-Feed-Einträge
                $NewsItemsArray = $xmlResultArray['rss']['channel']['item'];
                $ResultNewsArray = searchNewsItems($NewsItemsArray);
            }
        } else {
            echo 'Es hat etwas nicht funktioniert beim Auslesen des XML-Feeds. Evtl ist der Feed-Server down oder                
     nicht richtig konfiguriert';
        }
        //TestFinal
        echo "<pre>";
        echo var_dump($ResultNewsArray);
        echo "</pre>";
        //$code = "<pre>"; $code += var_dump($ResultNewsArray); $code += "</pre>"; echo $code;


        //Beispiel fürs returnen
        return $this->render('googleNews/googleNews.html.twig', array(
            'newsItemsArray' => $ResultNewsArray,
        ));

        //RESULT
        //return $ResultNewsArray;
        //<img src={{ newsItem.imgSrc}} />
        /*twig
        {% block body %}
    <h1>Google News Feed</h1>

    {{ dump() }}

    <ul>
        {% for newsItem in newsItemsArray %}
            <li>{{ newsItem.title }}
                <ul>

                    {{ newsItem.shortDescription }}

                </ul>
            </li>

        {% endfor %}
    </ul>
{% endblock %}
        */

    }
}
